# Prometheus and Grafana with Docker
# Lab 03: Prometheus metrics and Grafana Dashboard


# Preparations
- Verify that prometheuse and grafana are up and running:
    - Browse to http://serverip_or_hostname:9090 
    - Browse to http://serverip_or_hostname:3000

---

# Tasks

 - Prometheus metrics 

 - Grafana Dashboard

--- 
 
## Prometheus metrics 

&nbsp;

- To get list of metrics browse to: http://serverip_or_hostname:9090/metrics
- list of metrics accomapnied with theyre desciption
<img alt="Image 1" src="images/prometheus_1.png" width="100%" height="100%">

&nbsp;

- Graph for metric: go_memstats_alloc_bytes
- Add go_memstats_alloc_bytes to the expression box and select graph 
<img alt="Image 2" src="images/prometheus_2.png" width="100%" height="100%">

## Grafana Dashboard
- Navigate to grafana Home - Add new dashboard:
<img alt="Image 3" src="images/grafana_1.png" width="75%" height="100%">
<img alt="Image 4" src="images/grafana_2.png" width="75%" height="100%">

- Add metric: go_memstats_alloc_bytes to the data source editor and save
<img alt="Image 5" src="images/grafana_3.png" width="75%" height="100%">
<img alt="Image 6" src="images/grafana_4.png" width="75%" height="100%">
<img alt="Image 6" src="images/grafana_5.png" width="75%" height="100%">
